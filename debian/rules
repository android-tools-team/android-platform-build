#!/usr/bin/make -f

# Security Hardening
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

include /usr/share/dpkg/architecture.mk
include /usr/share/javahelper/java-vars.mk

export DEB_HOST_MULTIARCH
export DEB_CPPFLAGS_MAINT_APPEND = -DNDEBUG -I/usr/include/android
export DEB_LDFLAGS_MAINT_APPEND = -fPIC

export CLASSPATH = /usr/share/java/bcprov.jar:/usr/share/java/bcpkix.jar:/usr/share/java/apksig.jar

MANPAGES = \
  debian/java-event-log-tags.1 \
  debian/makeparallel.1 \
  debian/merge-event-log-tags.1 \
  debian/signapk.1 \
  debian/signtos.1 \
  debian/zipalign.1 \
  debian/ziptime.1 \

debian/%.1: debian/%.1.md
	pandoc --standalone --from=markdown-smart --to=man --output=$@ $<

all-manpages: $(MANPAGES)
	@echo "Generated all manpages."

clean-manpages:
	$(RM) $(MANPAGES)

makeparallel: debian/makeparallel.mk
	dh_auto_build --buildsystem=makefile -- --file $<

zipalign: debian/zipalign.mk
	dh_auto_build --buildsystem=makefile -- --file $<

ziptime: debian/ziptime.mk
	dh_auto_build --buildsystem=makefile -- --file $<

signapk.jar:
	jh_build --javacopts="-encoding UTF-8 -source 1.9 -target 1.9" --no-javadoc --main=com.android.signapk.SignApk $@ tools/signapk/

signtos.jar:
	jh_build --javacopts="-encoding UTF-8 -source 1.9 -target 1.9" --no-javadoc --main=com.android.signtos.SignTos $@ tools/signtos/

%:
	dh $@ --with javahelper

override_dh_auto_build-arch: makeparallel zipalign ziptime

override_dh_auto_build-indep: signapk.jar signtos.jar
